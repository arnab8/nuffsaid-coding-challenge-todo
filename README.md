# nuffsaid-interview-todo

Build a simple TODO application

## Technology to be used

You can use [React](https://reactjs.org/), [lodash](https://lodash.com/docs), [styled-components](https://styled-components.com/) and [Material UI](http://www.material-ui.com/#/)

## UI Look and feel requirement

<img width="278" alt="screen shot 2018-03-09 at 8 34 47 pm" src="https://user-images.githubusercontent.com/446864/37237994-d3b96f58-23d9-11e8-8eff-8e375488d16b.png">


## Video demonstration of the application
![todo-app](https://user-images.githubusercontent.com/446864/37237991-c519b566-23d9-11e8-8ec0-3ac9c05c1e5e.gif)

## Behavior

- A simple text box to enter todo text
- Checkbox selection to select/deselect TODO item
- Checkbox to show/hide completed TODO
- Delete icon
- Keyboard 'enter' command should add the TODO item to the list

## Submission

You have to fork this project and checkin all your changes into your fork. Once done, send your fork link to the person who contacted you for the interview

## Note

- You are not allowed to use other frameworks or libraries
- Test your application to the degree that you feel comfortable with. No specific testing frameworks are required.
- Use functional components. If possible, change MessageList to functional component
- Your implementation should be more like how you do it for any production grade application development

***Applicants are provided this challenge with no expectation on deadline. Please take the time you need to complete the challenge to the best of your ability.***
